import ChildPage from './MainPage/ChildPage';
import LoginPage from './LoginPage/LoginPage';
import { useCookies } from 'react-cookie';
import 'antd/dist/antd.css';

function App() {
  const [cookies, setCookie] = useCookies(['keyCookie']);
  return (
    <div>
      { cookies.apiSecretKeyCookie && cookies.apiKeyCookie ? 
        <ChildPage/> : <LoginPage/> 
      }
    </div>
  );
}

export default App;
