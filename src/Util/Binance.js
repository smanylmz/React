const BASE = 'https://api.binance.com'

const makeQueryString = q =>
  q
    ? `?${Object.keys(q)
        .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(q[k])}`)
        .join('&')}`
    : ''

const sendResult = call =>
  call.then(res => Promise.all([res, res.json()])).then(([res, json]) => {
    if (!res.ok) {
      const error = new Error(json.msg || `${res.status} ${res.statusText}`)
      error.code = json.code
      throw error
    }

    return json
  })

const checkParams = (name, payload, requires = []) => {
  if (!payload) {
    throw new Error('You need to pass a payload object.')
  }

  requires.forEach(r => {
    if (!payload[r] && isNaN(payload[r])) {
      throw new Error(`Method ${name} requires ${r} parameter.`)
    }
  })

  return true
}

const publicCall = (path, data, method = 'GET', headers = {}) =>
  sendResult(
    fetch(`${BASE}/api${path}${makeQueryString(data)}`, {
      method,
      json: true,
      headers,
    }),
  )

const keyCall = ({ apiKey }) => (path, data, method = 'GET') => {
  if (!apiKey) {
    throw new Error('You need to pass an API key to make this call.')
  }

  return publicCall(path, data, method, {
    'X-MBX-APIKEY': apiKey,
  })
}

const privateCall = ({ apiKey, apiSecret }) => ( path, data = {}, method = 'GET', noData = false, noExtra = false ) => {
    if (!apiKey || !apiSecret) {
      throw new Error('You need to pass an API key and secret to make authenticated calls.')
    }
  
    return (data && data.useServerTime ? publicCall('/v1/time').then(r => r.serverTime) : Promise.resolve(Date.now()))
    .then(timestamp => {
      if (data) {
        delete data.useServerTime
      }
  
      var crypto = require("crypto");
      const signature = crypto
      .createHmac('sha256', apiSecret)
      .update(makeQueryString({ ...data, timestamp }).substr(1))
      .digest('hex');
  
      const newData = noExtra ? data : { ...data, timestamp, signature }
  
      return sendResult(
        fetch(`${BASE}${path.includes('/wapi') || path.includes('/sapi') ? '' : '/api'}${path}${noData ? '' : makeQueryString(newData)}`,
          {
            method,
            headers: { 'X-MBX-APIKEY': apiKey },
            json: true,
          },
        ),
      )
    })
  }

  export default opts => {
    const pCall = privateCall(opts)
    const kCall = keyCall(opts)
  
    return {
      ping: () => publicCall('/v1/ping').then(() => true),
      time: () => publicCall('/v1/time').then(r => r.serverTime),

      getAll: payload => pCall('/sapi/v1/capital/config/getall', payload),
      klines: payload => checkParams('klines', payload, ['symbol','interval','startTime','endTime']) && pCall('/v3/klines', payload, 'GET', false, true),
    }
  }
