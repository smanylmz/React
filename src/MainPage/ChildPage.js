import React, { useState, useEffect, useCallback } from 'react'
import { createChart, CrosshairMode, LineStyle } from 'lightweight-charts';
import { useCookies } from 'react-cookie';
import AutoCompleteComponent from '../ComponentPage/AutoComplateComponent';
import { Table, Button, Row, Col } from 'antd';
import Binance from '../Util/Binance'

const columns = [
  {
    title: 'Coin Name',
    dataIndex: 'coinName'
  },
  {
    title: 'Symbol',
    dataIndex: 'symbol'
  },
  {
    title: 'Market Cap (USD)',
    dataIndex: 'marketCap'
  },
  {
    title: 'Action',
    key: 'action',
    sorter: true,
    render: () => (
      <Button type="primary"> Delete </Button>
    ),
  },
];

const ChildPage = props  =>  {
  const [chartArray, setChartArray] = useState([]);
  const [cookies, setCookie, removeCookie] = useCookies(['keyCookie']);

  const client = Binance({ apiKey : cookies.apiKeyCookie, apiSecret : cookies.apiSecretKeyCookie });

  var ws = new WebSocket('wss://stream.binance.com:9443/ws/ethusdt@kline_1m');
  ws.onmessage  = (jsonString) => {
    for(var i = 0; i < chartArray.length; i++){
      var jsonValue = JSON.parse(jsonString.data);
      var currentBar = {
          open: jsonValue.k.o,
          high: jsonValue.k.h,
          low: jsonValue.k.l,
          close: jsonValue.k.c,
          time: jsonValue.k.t,
      };
      if(chartArray[i].updateFunction != null){
        chartArray[i].updateFunction(currentBar);
      }
    }
  };

  function addPriceLine(detail, lineColor, lineTitle, linePrice){
    var minPriceLine = {
      price: linePrice,
      color: lineColor,
      lineWidth: 2,
      lineStyle: LineStyle.Solid,
      axisLabelVisible: true,
      title: lineTitle,
    };
    detail.lineArray.push(detail.candleSeries.createPriceLine(minPriceLine));
  }

  function addCharts(chartId, coinName, coinSymbol) {
    setChartArray(prevState => [...prevState, 
      {
        key: chartArray.length + 1,
        coinName: coinName,
        symbol: coinSymbol,
        marketCap: 0,
        chart: null, 
        candleSeries: null,
        lightWeightChartId: chartId + chartArray.length + 1,
        updateFunction: null,
        lineArray : []
      }]);
    }

  function addNewChart() {
    addCharts("denemeChart", "Bitcoin", "BTC");
  }

  function removeCookies() {
    removeCookie('apiKeyCookie');
    removeCookie('apiSecretKeyCookie');
    window.location.reload(false);
  }

  const elemRef = useCallback((node, detail) => {
    if(node != null && detail.chart == null && detail.candleSeries == null) {
      var newChart = createChart(detail.lightWeightChartId, {
        width: 1024,
        height: 768,
        crosshair: {
          mode: CrosshairMode.Normal,
        },
      });

      detail.chart = newChart;
      detail.candleSeries = newChart.addCandlestickSeries();
      detail.updateFunction = (currentBar) => {
        detail.candleSeries.update(currentBar);
      }
      addPriceLine(detail, '#be1238',"Deneme Line", 1840);
    }
  },[]);

  useEffect(() => {
    async function fetchMyAPI() {
      const res2 = await client.klines({symbol: 'USDTTRY', interval: '1m', startTime: 1612108487267, endTime: 1612108547267});
      console.log(res2);

      const res = await client.getAll();
      console.log(res);
    }
    fetchMyAPI();
  }, [])

  const onSelect = (data) => {
    console.log('onSelect', data);
  };

  return (
      <Row>
        <Col span={24}>
          <Table
              pagination={{ position: ['none', 'bottomRight'] }}
              columns={columns}
              dataSource={chartArray}
              bordered={true}
              hasData={true}
              expandable={{
                expandedRowRender: record => 
                <Row>
                  <Col span={3}/>
                  <Col span={18}>
                    <div id={record.lightWeightChartId} className='LightweightChart' ref={el => elemRef(el, record)}/>
                  </Col>
                  <Col span={3}/>
                </Row>
              }}
          />
        </Col>
        <Col span={24}>
          <AutoCompleteComponent autoCompleteItems={["12","456"]} onSelect={onSelect} />
          <Button type="primary" onClick={() => addNewChart()}>Ekle</Button>
          <Button type="primary" onClick={() => removeCookies()}>Çıkış Yap</Button>
        </Col>
      </Row>

      
      //<div>
      //    { props && props.containerId && <div id={props.containerId} className={'LightweightChart'} /> }
      //</div>
  );
}

export default ChildPage;