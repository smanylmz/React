import React, { useState, useEffect } from 'react'
import { AutoComplete } from 'antd';
const { Option } = AutoComplete;

const AutoCompleteComponent = props => {
    const [autoCompleteItems, setAutoCompleteItems] = useState([]);
    const [result, setResult] = useState([]);

    useEffect(() => {
        setAutoCompleteItems(props.autoCompleteItems);
        setResult([]);
    }, [props])

    const onSearch = (value) => {
        let res = [];
        if (!value) {
          res = [];
        } else {
            res = autoCompleteItems.filter(item => item.indexOf(value) != -1).map(item => item);
            if(res.length > 0)
                res = res.slice(0, res.length > 3 ? 3 : res.length).map(item => item);
        }
        setResult(res);
    };
    
    return (
        <AutoComplete
        style={{ width: !props.width ? 200 : props.width }}
        onSelect={props.onSelect}
        onSearch={onSearch}
        placeholder="input here">
            {result.map((item) => (
              <Option key={item} value={item}>
                {item}
              </Option>
            ))}
          </AutoComplete>
    );
}

export default AutoCompleteComponent;