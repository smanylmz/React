//api/v3/klines
//sapi/v1/capital/config/getall

  function GET(URL, props) {
      var Parameters = []
      if(props.useTimeStamp){   
        Parameters.push("timestamp=" + ((new Date()).getTime() - 3));
      }
      if(props.useSign){   
        var crypto = require("crypto");
        var sign = crypto.createHmac('sha256', props.apiSecretKey).update(Parameters.join("&")).digest('hex');
        Parameters.push("signature=" + sign);
      }

      fetch("https://api.binance.com/" + URL + (Parameters.length > 0 ? ("?" +  Parameters.join("&")) : ""), {
        method: 'GET',
        headers: {
            'X-MBX-APIKEY': cookies.apiKeyCookie,
        }
        }).then(response => {
        if (response.ok) {
            return response.json()
        }
        console.log(response.statusText);
        })
        .then(response => { return response.json() })
        .then(data => console.log({ data }))
        .catch(error => console.error({ error }));
  }

fetch("https://api.binance.com/api/v3/klines?symbol=USDTTRY&interval=1m&startTime=1612108487267&endTime=1612108547267")
.then(response => {
  return response.json()
})
.then(data => console.log({ data }))
.catch(error => console.error({ error }));
